/***

- AUTHOR: @Lirrums
- LANGUAGE: Golang
- TOPIC: Algorithms with Go.
- GITHUB: [https://gitlab.com/linacastrodev/]

***/
package main

import (
	"fmt"
)

func main() {
	// create a algorithm that enter two numbers and show the average of both
	twonumbers()
	// create a algorithm that enter a integer number and show to the month corresponding. Example January = 1
	months()
	// create a algorithm for read three integers numbers each differents and determinate the number higher to the three.
	higher()
	// create a algorithm that need get the average of a students starting of your three notes partials.
	notes_partial()
	// create a algorithm that enter the age from two persons and show in screen the age of the higher.
	age_people()
}
func twonumbers() {
	// declared variable integer
	var numberone int
	var numbertwo int
	// ask about the enter number
	fmt.Println("create a algorithm that enter two numbers and show the average of both")
	fmt.Println("Enter First Number:")
	// read the data
	fmt.Scanf("%d\n", &numberone)
	fmt.Println("Enter Second Number: ")
	fmt.Scanf("%d\n", &numbertwo)
	// show the data
	var result = numberone * numbertwo / 2
	fmt.Printf("This is the final result: %d\n", result)
}
func months() {
	//declared variable
	var number int
	// ask number
	fmt.Println("create a algorithm that enter a integer number and show to the month corresponding. Example January = 1")
	fmt.Println("Enter a Number")
	// read number
	fmt.Scanf("%v\n", &number)
	// logic
	switch number {
	case 1:
		fmt.Println("January")
	case 2:
		fmt.Println("February")
	case 3:
		fmt.Println("March")
	case 4:
		fmt.Println("April")
	case 5:
		fmt.Println("May")
	case 6:
		fmt.Println("June")
	case 7:
		fmt.Println("July")
	case 8:
		fmt.Println("August")
	case 9:
		fmt.Println("September")
	case 10:
		fmt.Println("October")
	case 11:
		fmt.Println("November")
	case 12:
		fmt.Println("December")
	}
}
func higher() {
	// declared variable integer
	var number_one int
	var number_two int
	var number_three int
	fmt.Println("create a algorithm for read three integers numbers each differents and determinate the number higher to the three.")
	fmt.Println("Enter First Number:")
	// read the data
	fmt.Scanf("%d\n", &number_one)
	// enter data
	fmt.Println("Enter Second Number: ")
	// enter data
	fmt.Scanf("%d\n", &number_two)
	// read data
	fmt.Println("Enter Thirty Number")
	fmt.Scanf("%d\n", &number_three)
	//logic
	if number_one > number_two && number_one > number_three {
		fmt.Println("the number higher is: ", number_one)
	} else if number_two > number_one && number_two > number_three {
		fmt.Println("the number higher is: ", number_two)
	} else {
		fmt.Println("the number higher is: ", number_three)
	}

}
func notes_partial() {
	// declared variable integer
	var first_note int
	var second_note int
	var third_note int
	//enter data
	fmt.Println("create a algorithm that need get the average of a students starting of your three notes partials.")
	fmt.Println("Enter first note")
	//read data
	fmt.Scanf("%d\n", &first_note)
	//enter data
	fmt.Println("Enter second note")
	//read data
	fmt.Scanf("%d\n", &second_note)
	//read data
	fmt.Println("Enter third note")
	//enter data
	fmt.Scanf("%d\n", &third_note)
	//read data

	//logic
	result := first_note * second_note * third_note / 3
	fmt.Printf("the average about your notes is: %d\n", result)
}
func age_people() {
	// declared variable integer
	var age_people_one int
	var age_people_two int
	// enter data
	fmt.Println("create a algorithm that enter the age from two persons and show in screen the age of the higher.")
	fmt.Println("Enter the age of first person: ")
	// read data
	fmt.Scanf("%d\n", &age_people_one)
	// enter data
	fmt.Println("Enter the age of second person: ")
	// read data
	fmt.Scanf("%d\n", &age_people_two)
	//logic
	if age_people_one > age_people_two {
		fmt.Println("the first person is the higher")
	} else if age_people_one == age_people_two {
		fmt.Println("the both person is the higher")
	} else {
		fmt.Println("the second person is the higher")
	}

}
