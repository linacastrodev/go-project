# Algorithms Golang

_This project containt various Algorithms with golang, really this Algorithms are so easy, but in this time I'm learning about this big language._

## Installation and initialization 🔧

_You need enter into the directory in the application, to run the next command: 

```
go run algorithms.go
```

_Or yo can use the next command:_

```
go build algorithms.go
```

## Built with 🛠️

- [Golang](https://www.golang.org/) - Programming Language


## Authors ✒️

- **Lina Castro** - _Software Developer_ - [linacastrodev](https://gilab.com/linacastrodev)

## License 📄

This project is under the License (MIT)